package com.example.myspringsecurityproject.member.domain;

import com.example.myspringsecurityproject.common.domain.MyVO;

public class MemberVO extends MyVO {

    String email;
    String authCode;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }
}
