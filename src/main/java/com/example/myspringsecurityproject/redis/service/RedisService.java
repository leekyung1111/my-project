package com.example.myspringsecurityproject.redis.service;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 파일명 : RedisService
 * 작성일자 : 2024-02-15
 * 작성자 (@author 이경율)
 * 참조 Class File (@see File )
 * 설명 : RedisService
 * 수정 내역
 * ---------------------------------------
 * | 수정일자 | 수정자 | 수정내역 |
 * ---------------------------------------
 * 2024-02-15 | 이경율 | 최초 생성 |
 */
@Component
public class RedisService {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * key, value 저장
     * @param key
     * @param data
     */
    public void setValues(String key, String data) {
        ValueOperations<String, Object> values = redisTemplate.opsForValue();
        values.set(key, data);
    }

    public void setValues(String key, String data, Duration duration) {
        ValueOperations<String, Object> values = redisTemplate.opsForValue();
        values.set(key, data, duration);
    }

    /**
     * key 값으로 value 가져오기
     * @param key
     * @return
     */
    @Transactional(readOnly = true)
    public String getValues(String key) {
        ValueOperations<String, Object> values = redisTemplate.opsForValue();
        if (values.get(key) == null) {
            return "false";
        }

        return (String) values.get(key);
    }

    /**
     * key 값으로 value 삭제
     * @param key
     */
    public void deleteValues(String key) {
        redisTemplate.delete(key);
    }

    /**
     * key 값으로 value 만료시간 설정
     * @param key
     * @param timeout
     */
    public void expireValues(String key, int timeout) {
        redisTemplate.expire(key, timeout, TimeUnit.MILLISECONDS);
    }

    /**
     * key, hash 저장
     * @param key
     * @param data
     */
    public void setHashOps(String key, Map<String, String> data) {
        HashOperations<String, Object, Object> values = redisTemplate.opsForHash();
        values.putAll(key, data);
    }

    /**
     * key 값으로 hash 가져오기
     * @param key
     * @param hashKey
     * @return
     */
    @Transactional(readOnly = true)
    public String getHashOps(String key, String hashKey) {
        HashOperations<String, Object, Object> values = redisTemplate.opsForHash();
        return Boolean.TRUE.equals(values.hasKey(key, hashKey)) ? (String) redisTemplate.opsForHash().get(key, hashKey) : "";
    }

    /**
     * key 값으로 hash 삭제
     * @param key
     * @param hashKey
     */
    public void deleteHashOps(String key, String hashKey) {
        HashOperations<String, Object, Object> values = redisTemplate.opsForHash();
        values.delete(key, hashKey);
    }

    /**
     * 존재 여부 확인
     * @param value
     * @return
     */
    public boolean checkExistsValue(String value) {
        return !value.equals("false");
    }

}
