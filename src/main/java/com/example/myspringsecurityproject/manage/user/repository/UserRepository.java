package com.example.myspringsecurityproject.manage.user.repository;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import com.example.myspringsecurityproject.manage.user.domain.UserVO;

@Repository
@Mapper
public interface UserRepository {

    /**
     * 사용자 추가
     * @param params
     * @return
     */
    int insertUser(UserVO params);

    /**
     * OAuth 정보 추가
     * @param params
     * @return
     */
    int insertOAuthUserInfo(UserVO params);

    /**
     * 중복 아이디 체크
     * @param params
     * @return
     */
    int getDuplicationIdCnt(String params);

    /**
     * 비밀번호 변경
     * @param params
     * @return
     */
    int updateUserPwd(UserVO params);

    /**
     * 사용자 조회 by email
     * @param params
     * @return
     */
    UserVO selectUser(UserVO params);
}
