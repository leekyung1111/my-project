package com.example.myspringsecurityproject.manage.user.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.myspringsecurityproject.common.constant.MyConstant;
import com.example.myspringsecurityproject.common.exception.MyException;
import com.example.myspringsecurityproject.manage.user.domain.UserVO;
import com.example.myspringsecurityproject.manage.user.repository.UserRepository;
import com.example.myspringsecurityproject.manage.user.service.UserService;

/**
 * 파일명 : UserServiceImpl
 * 작성일자 : 2023.12.25
 * 작성자 (@author 이경율)
 * 참조 Class File (@see kr.co.familidata.manage.user.service.impl.UserServiceImpl)
 * 설명 : 사용자 서비스 구현체
 * 수정내역
 * ---------------------------------------
 * | 수정일자 | 수정자 | 수정내역 |
 * ---------------------------------------
 * 2023.12.25  | 이경율 | 최초 생성 |
 */
@Service
@Transactional(rollbackFor = MyException.class)
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository repository;

    @Override
    public int getDuplicationIdCnt(String param) throws MyException {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "getDuplicationIdCnt", param);
        }

        return repository.getDuplicationIdCnt(param);
    }

    /**
     * 비밀번호 변경
     *
     * @param userVO
     * @return
     */
    @Override
    public int updateUserPwd(UserVO userVO) throws MyException {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "updateUserPwd", userVO);
        }

        return repository.updateUserPwd(userVO);

    }

    /**
     * 사용자 조회
     * @param params
     */
    @Override
    public UserVO selectUser(UserVO params) {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "selectUser", params);
        }

        return repository.selectUser(params);
    }
}
