package com.example.myspringsecurityproject.manage.user.service;

import com.example.myspringsecurityproject.common.exception.MyException;
import com.example.myspringsecurityproject.manage.user.domain.UserVO;

/**
 * 파일명 : UserService
 * 작성일자 : 2023.12.25
 * 작성자 (@author 이경율)
 * 참조 Class File (@see kr.co.familidata.manage.user.service.UserService)
 * 설명 : User service
 * 수정내역
 * ---------------------------------------
 * | 수정일자 | 수정자 | 수정내역 |
 * ---------------------------------------
 */
public interface UserService {

    int getDuplicationIdCnt(String param) throws MyException;

    /**
     * 비밀번호 변경
     * @param userVO
     * @return
     */
    int updateUserPwd(UserVO userVO) throws MyException;

    /**
     * 사용자 조회
     * @param params
     * @return
     */
    UserVO selectUser(UserVO params);
}
