package com.example.myspringsecurityproject.sample.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.myspringsecurityproject.common.constant.MyConstant;

@Controller
@RequestMapping("/sample")
public class SampleController {

    private static final Logger logger  = LoggerFactory.getLogger(SampleController.class);

    @GetMapping("/tables")
    public String sampleTable() {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "sampleTable", "");
        }

        return "/sample/tables";
    }
}
