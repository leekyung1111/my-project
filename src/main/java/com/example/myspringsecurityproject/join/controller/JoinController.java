package com.example.myspringsecurityproject.join.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.myspringsecurityproject.common.constant.MyConstant;
import com.example.myspringsecurityproject.common.exception.MyErrorCode;
import com.example.myspringsecurityproject.common.exception.MyException;
import com.example.myspringsecurityproject.join.service.JoinService;
import com.example.myspringsecurityproject.manage.user.domain.UserVO;
import com.example.myspringsecurityproject.manage.user.service.UserService;
import com.example.myspringsecurityproject.member.domain.MemberVO;

/**
 * 파일명 : JoinController
 * 작성일자 : 2023.12.25
 * 작성자 (@author 이경율)
 * 참조 Class File (@see kr.co.familidata.join.controller.JoinController)
 * 설명 : 회원가입 컨트롤러
 * 수정내역
 * ---------------------------------------
 * | 수정일자 | 수정자 | 수정내역 |
 * ---------------------------------------
 *  2023.12.25  | 이경율 | 최초 생성 |
 */
@Controller
@RequestMapping("/join")
public class JoinController {

    private static final Logger logger = LoggerFactory.getLogger(JoinController.class);

    @Autowired
    private JoinService service;

    @Autowired
    private UserService userService;

    /**
     * <pre>
     * 회원가입 신청 화면
     * </pre>
     * @param model
     * @param request
     * @return
     */
    @GetMapping("/register")
    public String join(Model model, HttpServletRequest request) {

        if (request.getAttribute("isOAuth") != null && (Boolean)request.getAttribute("isOAuth")) {
            model.addAttribute("isOAuth", true);
            model.addAttribute("provider", request.getAttribute("provider"));
            model.addAttribute("providerUserId", request.getAttribute("providerUserId"));
            model.addAttribute("name", request.getAttribute("name"));
            model.addAttribute("email", request.getAttribute("email"));
            model.addAttribute("oauthMessage", request.getAttribute("oauthMessage"));
        } else {
            model.addAttribute("isOAuth", false);
        }

        if (request.getAttribute("error") != null && (Boolean)request.getAttribute("error")) {
            model.addAttribute("error", true);
            model.addAttribute("errorMessage", request.getAttribute("message"));
        } else {
            model.addAttribute("error", false);
        }

        return "/join/register";
    }

    /**
     * <pre>
     * 회원가입 신청 저장
     * </pre>
     * @param param
     * @param request
     * @param response
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @PostMapping("/register")
    @ResponseBody
    public int joinPost(@RequestBody UserVO param, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.info("param: {}", param);

        if (!validateExcution(param)) {
            throw new MyException(MyErrorCode.BAD_REQUEST_EXCEPTION);
        }

        service.saveJoinData(param);

        return 0;
    }

    /**
     * <pre>
     * 이메일 인증 링크 전송
     * </pre>
     * @param param
     * @return
     * @throws MyException
     */
    @PostMapping("/email/auth/sendLink")
    @ResponseBody
    public Map<String, Object> sendEmailAuthLink(@RequestBody UserVO param) throws MyException {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "sendEmailAuthLink", param);
        }

        return service.sendEmailAuthLink(param);
    }

    /**
     * <pre>
     * 인증결과 확인
     * </pre>
     * @param param
     * @param model
     * @return
     */
    @GetMapping("/email/auth/verificationCheck")
    public String emailAuthVerificationCheck(MemberVO param, Model model) {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "emailAuthVerificationCheck", param);
        }

        Map<String, Object> result = service.checkCode(param);

        model.addAttribute("result", result.get("message"));

        return "/join/emailAuthResult";
    }

    @PostMapping("/email/auth/getEmailAuthStatus")
    @ResponseBody
    public Map<String, Object> emailAuthVerificationStatusCheck(@RequestBody MemberVO param) {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "getEmailAuthStatus", param);
        }

        return service.getEmailAuthStatus(param);
    }

    /**
     * <pre>
     * validateExcution
     * </pre>
     * @param param
     * @return
     */
    private boolean validateExcution(UserVO param) {

        if (param.getLoginId() == null || param.getLoginId().equals("")) {
            return false;
        }
        if (param.getUserPwd() == null || param.getUserPwd().equals("")) {
            return false;
        }
        if (param.getUserName() == null || param.getUserName().equals("")) {
            return false;
        }
        if (param.getEmail() == null || param.getEmail().equals("")) {
            return false;
        }
        if (!duplicateCheck(param.getLoginId())) {
            throw new MyException(MyErrorCode.USER_EXIST_EXCEPTION);
        }

        return true;

    }

    /**
     * <pre>
     * 중복체크
     * </pre>
     * @param param
     * @return
     */
    private boolean duplicateCheck(String param) {
        return userService.getDuplicationIdCnt(param) <= 0;
    }
}
