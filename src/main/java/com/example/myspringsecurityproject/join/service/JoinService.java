package com.example.myspringsecurityproject.join.service;

import java.util.Map;

import com.example.myspringsecurityproject.common.exception.MyException;
import com.example.myspringsecurityproject.manage.user.domain.UserVO;
import com.example.myspringsecurityproject.member.domain.MemberVO;

/**
 * 파일명 : JoinService
 * 작성일자 : 2023.12.25
 * 작성자 (@author 이경율)
 * 참조 Class File (@see kr.co.familidata.join.service.JoinService)
 * 설명 : 회원가입 서비스
 * 수정내역
 * ---------------------------------------
 * | 수정일자 | 수정자 | 수정내역 |
 * ---------------------------------------
 *  2023.12.25  | 이경율 | 최초 생성 |
 */
public interface JoinService {

    /**
     * <pre>
     * 회원가입 신청 저장
     * </pre>
     * @param params
     * @return
     */
    int saveJoinData(UserVO params) throws MyException;

    /**
     * 이메일 인증 링크 전송
     * @param param
     * @return
     */
    Map<String, Object> sendEmailAuthLink(UserVO param);

    /**
     * 인증코드 확인
     * @param param
     * @return
     */
    Map<String, Object> checkCode(MemberVO param);

    /**
     * 인증코드 만료여부 확인
     * @param param
     * @return
     */
    Map<String, Object> getEmailAuthStatus(MemberVO param);
}
