package com.example.myspringsecurityproject.join.service.impl;

import java.net.InetAddress;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.myspringsecurityproject.common.constant.MyConstant;
import com.example.myspringsecurityproject.common.exception.MyErrorCode;
import com.example.myspringsecurityproject.common.exception.MyException;
import com.example.myspringsecurityproject.join.repository.JoinRepository;
import com.example.myspringsecurityproject.join.service.JoinService;
import com.example.myspringsecurityproject.manage.user.domain.UserVO;
import com.example.myspringsecurityproject.manage.user.repository.UserRepository;
import com.example.myspringsecurityproject.member.domain.MemberVO;
import com.example.myspringsecurityproject.redis.service.RedisService;

/**
 * 파일명 : JoinServiceImpl
 * 작성일자 : 2023.12.25
 * 작성자 (@author 이경율)
 * 참조 Class File (@see kr.co.familidata.join.service.impl.JoinServiceImpl)
 * 설명 : 회원가입 서비스 구현체
 * 수정내역
 * ---------------------------------------
 * | 수정일자 | 수정자 | 수정내역 |
 * ---------------------------------------
 * 2023.12.25  | 이경율 | 최초 생성 |
 */
@Service
@Transactional(rollbackFor = MyException.class)
public class JoinServiceImpl implements JoinService {

    private static final Logger logger = LoggerFactory.getLogger(JoinServiceImpl.class);
    private static final String AUTH_CODE = "authCode";
    private static final String IS_AUTH = "isAuth";

    @Autowired
    private JoinRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    JavaMailSender mailSender;

    @Autowired
    RedisService redisService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Value("${email.auth.endpoint}")
    private String authCheckEndpoint;
    @Value("${spring.mail.auth-code-expiration-millis}")
    private long authCodeExpirationMillis;

    /**
     * <pre>
     * 회원가입 신청 저장
     * </pre>
     *
     * @param params
     * @return
     */
    @Override
    public int saveJoinData(UserVO params) throws MyException {

        String newPassword = passwordEncoder.encode(params.getUserPwd());

        params.setUserPwd(newPassword);
        params.setLanguage("ko");

        logger.info("params: {}", params);

        userRepository.insertUser(params);

        if (params.getProvider() != null) {
            userRepository.insertOAuthUserInfo(params);
        }

        return 0;
    }

    /**
     * 이메일 인증 링크 전송
     *
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> sendEmailAuthLink(UserVO param) throws MyException {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "sendEmailAuthLink", param);
        }

        Map<String, Object> result = new HashMap<>();

        String email = param.getEmail();

        // 이메일 중복 확인
        UserVO userByEmail = userRepository.selectUser(param);
        if (userByEmail != null) {
            throw new MyException(MyErrorCode.DUPLICATION_EMAIL);
        }
        // 이메일 유효성 검사
        if (!isValidEmail(email)) {
            throw new MyException(MyErrorCode.INVALID_EMAIL);
        }

        // 이메일 인증 링크 전송
        String authCode = UUID.randomUUID().toString();
        String subject = "이메일 인증 링크";
        String text = getMailContents(authCode, email);
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setFrom("leekyung1111@gmail.com", "MyProject");
            helper.setTo(email);
            helper.setSubject(subject);
            helper.setText(text, true);
            mailSender.send(message);
        } catch (Exception e) {
            logger.error(MyConstant.LOG_PARAM, this.getClass().getName(), "sendEmailAuthLink", e);
            throw new MyException(MyErrorCode.MY_EXCEPTION);
        }

        // 인증코드 저장
        redisService.setValues(AUTH_CODE + email, authCode, Duration.ofMillis(authCodeExpirationMillis));
        // 인증유무 저장
        redisService.setValues(IS_AUTH + email, "N", Duration.ofMillis(authCodeExpirationMillis));

        result.put("email", email);
        result.put("authCode", authCode);

        return result;

    }

    /**
     * 인증코드 확인
     *
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> checkCode(MemberVO param) {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "checkCode", param);
        }

        Map<String, Object> result = new HashMap<>();

        String email = param.getEmail();
        String authCode = param.getAuthCode();

        String redisAuthCode = redisService.getValues(AUTH_CODE + email);
        String redisIsAuth = redisService.getValues(IS_AUTH + email);

        logger.info("redisAuthCode: {}", redisAuthCode);
        logger.info("redisIsAuth: {}", redisIsAuth);

        if (redisIsAuth.equals("Y")) {
            result.put("message", "이미 인증된 이메일입니다.");
            return result;
        }
        if (authCode.equals(redisAuthCode)) {
            redisService.setValues(IS_AUTH + email, "Y", Duration.ofMillis(authCodeExpirationMillis));
            result.put("message", "인증되었습니다. \n 회원가입을 진행해주세요.");
            return result;
        } else {
            result.put("message", "인증시간이 만료되었습니다.");
            return result;
        }
    }

    /**
     * 인증코드 만료여부 확인
     * @param param
     * @return
     */
    @Override
    public Map<String, Object> getEmailAuthStatus(MemberVO param) {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "getEmailAuthStatus", param);
        }

        String email = param.getEmail();
        String isAuth = redisService.getValues(IS_AUTH + email);

        Map<String, Object> result = new HashMap<>();

        if (isAuth.equals("Y")) {
            result.put("result", true);
            result.put("message", "인증되었습니다. \n 회원가입을 진행해주세요.");
            return result;
        } else {
            result.put("result", false);
            result.put("message", "인증되지 않았습니다.");
            return result;
        }
    }

    /**
     * 이메일 유효성 검사
     *
     * @param email
     * @return
     */
    public boolean isValidEmail(String email) {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "isValidEmail", email);
        }

        try {
            InternetAddress internetAddress = new InternetAddress(email);
            internetAddress.validate();
            String domain = email.split("@")[1];
            return isExistDomain(domain);
        } catch (Exception e) {
            throw new MyException(MyErrorCode.INVALID_EMAIL);
        }
    }

    /**
     * 도메인 존재 여부
     *
     * @param domain
     * @return
     */
    public boolean isExistDomain(String domain) {

        if (logger.isDebugEnabled()) {
            logger.debug(MyConstant.LOG_PARAM, this.getClass().getName(), "isExistDomain", domain);
        }

        try {
            InetAddress inetAddress = InetAddress.getByName(domain);
            return !inetAddress.getHostAddress().equals(domain);
        } catch (Exception e) {
            throw new MyException(MyErrorCode.INVALID_EMAIL);
        }
    }

    /**
     * 이메일 내용
     *
     * @param authCode
     * @param email
     * @return
     */
    private String getMailContents(String authCode, String email) {
        String contents = "";
        contents += "<div style='margin:100px;'>";
        contents += "<h1> 안녕하세요 My Project 입니다.</h1>";
        contents += "<br>";
        contents += "<p>가입을 위해 아래 인증링크를 클릭해주세요<p>";
        contents += "<br>";
        contents += "<a href=" + authCheckEndpoint + "?authCode=" + authCode + "&email=" + email + ">인증링크</a>";
        return contents;
    }
}
