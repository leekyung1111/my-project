
### 설치 및 실행

1. 저장소를 클론합니다.

    ```bash
    git clone https://gitlab.com/leekyung1111/my-project.git
    ```

2. intellij ide 

    - file - open - git clone directory
    - edit - run configuration - Modidfy options - Add VM options 내용 추가(Djava.net.preferIPv4Stack=true -Dspring.profiles.active=local)

3. server 구성
    - server는 ec2 centos7.9 사용
    - 아래는 home 아래에 구성한 directory구조이다.
    - admin과 centos는 사용자 directory
    - admin은 root로 실행하지 않기 위해서 사용자를 추가하였다.(adduser admin)
    - app 아래 projectd의 경우 서비스하는 포트만 달리하는 project를 2대 실행하고 ngix를 proxy server로 두고 load balancing test를 위하여 
    - https://velog.io/@lkr1111/TOMCATNGINX-WEBSERVER-%ED%99%98%EA%B2%BD%EA%B5%AC%EC%84%B1
    ```bash
    home
    .
    |-- admin
    |-- app
    |   |-- projectd
    |   |   |-- bin
    |   |   |-- conf
    |   |   |-- logs
    |   |   |-- temp
    |   |   |-- webapps
    |   |   `-- work
    |   |-- projectd2
    |       |-- bin
    |       |-- conf
    |       |-- logs
    |       |-- temp
    |       |-- webapps
    |       `-- work
    |-- centos
    `-- sw
        |-- apache-tomcat-8.5.83
        |   |-- BUILDING.txt
        |   |-- CONTRIBUTING.md
        |   |-- LICENSE
        |   |-- NOTICE
        |   |-- README.md
        |   |-- RELEASE-NOTES
        |   |-- RUNNING.txt
        |   |-- bin
        |   |-- conf
        |   |-- lib
        |   |-- logs
        |   |-- temp
        |   |-- webapps
        |   `-- work
        |-- java -> jdk1.8.0_181
        |-- jdk1.8.0_181
        |   |-- COPYRIGHT
        |   |-- LICENSE
        |   |-- README.html
        |   |-- THIRDPARTYLICENSEREADME-JAVAFX.txt
        |   |-- THIRDPARTYLICENSEREADME.txt
        |   |-- bin
        |   |-- include
        |   |-- javafx-src.zip
        |   |-- jre
        |   |-- lib
        |   |-- man
        |   |-- release
        |   `-- src.zip
        |-- monitor
        |   `-- prometheus.yml
        |-- postgres
        |   |-- docker-compose.yml
        |   |-- postgres-data
        |   `-- postgres-db-bkup
        `-- tomcat -> apache-tomcat-8.5.83
    ```
    ![Alt text](%E1%84%80%E1%85%B3%E1%84%85%E1%85%B5%E1%86%B71-1.png)

4. database 및 monitoring 도구 docker-compose로 실행
    - prometheus.yml 파일 생성
        ```bash
        global:
            scrape_interval: 15s

        scrape_configs:
        - job_name: 'prometheus'
            static_configs:
            - targets: ['172.31.2.158:9090']

        - job_name: 'server-status'
            static_configs:
            - targets: ['172.31.2.158:9100']

        - job_name: 'database-status'
            static_configs:
            - targets: ['172.31.2.158:9187']
        ```

    - docker-compose.yml 파일 생성

        ```bash
        version: '3.8'
        services:
        postgres_db:
            image: postgres:latest
            container_name: postgres_db
            restart: always
            environment:
            - POSTGRES_PASSWORD=password
            ports:
            - "5432:5432"
            volumes:
            - /home/sw/postgres/postgres-data:/var/lib/postgresql/data

        prometheus:
            image: prom/prometheus
            container_name: prometheus
            restart: always
            depends_on: [node-exporter, postgres-exporter]
            ports:
            - "9090:9090"
            volumes:
            - /home/sw/monitor/prometheus.yml:/etc/prometheus/prometheus.yml

        grafana:
            image: grafana/grafana
            container_name: grafana
            restart: always
            depends_on: [prometheus]
            ports:
            - "3000:3000"

        node-exporter:
            image: quay.io/prometheus/node-exporter
            container_name: node-exporter
            restart: always
            ports:
            - "9100:9100"
            network_mode: host

        postgres-exporter:
            image: quay.io/prometheuscommunity/postgres-exporter:v0.11.0
            container_name: postgres-exporter
            restart: always
            depends_on: [postgres_db]
            environment:
            - DATA_SOURCE_NAME=postgresql://postgres:password@@172.31.2.158:5432/postgres?sslmode=disable
            network_mode: host
        ```
    - docker-compose.yml 파일이 위치한 directory에서 실행
        ```bash
        docker-compose up -d
        ```
5. db 구성
    - docker container 접속 후 db생성 및 사용자 추가
        ```bash
        docker exec -it postgres_db bash
        su postgres
        psql
        CREATE DATABASE my_db TEMPLATE template0 LC_COLLATE 'C';
        CREATE USER my_user password ‘password’;
        ALTER DATABASE my_db OWNER TO my_user;
        GRANT ALL PRIVILEGES ON DATABASE my_db TO my_user;  
        ```
    - db 접속 후 tables.sql 실행
    https://gitlab.com/leekyung1111/my-project/-/blob/develop/tables.sql?ref_type=heads
6. monitoring 화면 조회
    - https://velog.io/@lkr1111/Monitoring-Postgresql-with-Grafana-Prometheus-in-Docker
    - https://velog.io/@lkr1111/Monitoring-Host-with-Grafana-Prometheus-in-Docker
    ![Alt text](<스크린샷 2024-03-11 오후 11.15.01.png>)
    ![Alt text](<스크린샷 2024-03-11 오후 11.15.17.png>)

4. 애플리케이션을 실행합니다.


## 기능

프로젝트의 주요 기능들을 설명합니다.

- 기능 1: 설명
- 기능 2: 설명
- ...

## 라이센스

이 프로젝트는 [라이센스 이름] 라이센스를 따릅니다. 자세한 내용은 [LICENSE.md](LICENSE.md) 파일을 참조하세요.